Source: mksh
Section: shells
Priority: optional
Maintainer: Thorsten Glaser <tg@mirbsd.de>
Origin: WTF
Bugs: mailto:wtf@mirbsd.org
Homepage: http://mirbsd.de/mksh
# We build-depend on dietlibc-dev, libklibc-dev, musl-tools for mksh-static
# and link it against glibc if neither is usable for a given architecture.
# The meat checks actual shell usability based on the architecture, whether
# we are in paranoid (just before the freeze/release) mode, and whether the
# testsuite is run to determine bad builds. Really, do run the testsuite!
# (Even then, versioned B-D may be used to remove known bogus versions.)
#
# Crossbuilding note: you can amend <!cross> dietlibc-dev, libklibc-dev and
# musl-tools, except the package contents will change (same with nocheck),
# so I don’t do it as the spec forbids that.
#
Build-Depends: debhelper-compat (= 13), file,
# # libc variants to link against
  dietlibc-dev [alpha amd64 arm arm64 armeb armel armhf hppa i386 ia64 mips mips64el mipsel powerpc powerpcspe ppc64 ppc64el s390 s390x sparc sparc64 x32],
  libklibc-dev [alpha amd64 arm64 armel armhf hppa i386 ia64 loong64 m68k mips mips64el mipsel powerpc powerpcspe ppc64 ppc64el riscv64 s390 s390x sh4 sparc sparc64 x32],
  musl-tools [amd64 arm64 armel armhf i386 loong64 m68k mips mips64el mipsel ppc64el riscv64 s390x sh4],
# # to run the testsuite
  bsdextrautils, ed
Standards-Version: 4.7.0
Rules-Requires-Root: no
VCS-git: https://evolvis.org/anonscm/git/alioth/wtf-mksh.git -b master
VCS-Browser: https://evolvis.org/plugins/scmgit/cgi-bin/gitweb.cgi?p=alioth/wtf-mksh.git;a=shortlog;h=refs/heads/master

Package: mksh
Architecture: any
Multi-Arch: foreign
Built-Using: ${mksh:L-B-U}
Static-Built-Using: ${mksh:S-B-U}
Depends: ${misc:Depends}, ${shlibs:Depends}, ed
Recommends: bc, jupp
Suggests: dash-mksh
Description: MirBSD Korn Shell
 mksh is the successor of the Public Domain Korn shell (pdksh),
 a Bourne/POSIX compatible shell which is largely similar to the
 original AT&T Korn Shell (ksh88/ksh93).
 It includes bug fixes and feature improvements, in order to produce a
 modern, robust shell good for interactive and especially script use.
 mksh has UTF-8 support (in string operations and the Emacs editing
 mode). The code has been cleaned up and simplified, bugs fixed,
 standards compliance added, and several enhancements (for extended
 compatibility to other modern shells, as well as a couple of its
 own) are available.
 This shell is Debian Policy 10.4 compliant and works as /bin/sh on
 Debian systems (use the /bin/lksh executable) and is a good rescue
 and initrd shell (consider the /bin/mksh-static executable).
 .
 The mksh binary is a complete, full-featured shell. It provides a
 “consistent across all platforms” guarantee, using 32-bit integers
 for arithmetics, possibly deviating from POSIX.
 .
 The mksh-static binary is a version of mksh, linked against klibc,
 musl, or dietlibc (if they exist for that Debian architecture and
 are usable) and optimised for small code size, for example for use
 on initrd or initramfs images, installation or rescue systems.
 Except for omitting some features to be smaller, it is similar to
 the mksh binary otherwise. Note the exact feature set may differ
 depending on which C library was used to compile it.
 .
 The lksh binary is a script shell based on mksh intended to run old
 ksh88 and pdksh scripts, but not for interactive use. When used as
 /bin/sh it follows POSIX most closely, including use of the host’s
 “long” C data type for arithmetics. It also contains kludges so it
 can run as /bin/sh on Debian beyond what Policy dictates, to work
 around bugs in maintainer scripts and LSB init scripts shipped by
 many packages, such as including a rudimentary printf(1) builtin,
 permitting a shell function to be named stop overriding the default
 alias, more loose interpretation of shell extglobs, etc.
 .
 A sample ~/.mkshrc is included in /usr/share/doc/mksh/examples and
 provided as /etc/mkshrc conffile, which is sourced by another file
 /etc/skel/.mkshrc users are recommended to copy into their home.
 .
 This is the WTF Edition with slightly differing defaults: ed will
 be installed via a dependency; jupp which is used as the default
 EDITOR and VISUAL via Recommends, as is bc; the C.UTF-8 locale is
 defaulted to; more sane/BSD-ish defaults are used.
 Bug reports go directly to the upstream developer.
